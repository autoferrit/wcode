// eslint-disable-next-line import/no-unresolved
import * as sapper from '@sapper/server';
import compression from 'compression';
import express from 'express';
// import helmet from "helmet";
import sirv from 'sirv';

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

const app = express();
app.use(compression({ threshold: 0 }));
app.use(sirv('static', { dev }));
// app.use(helmet());
app.use(sapper.middleware());

app.listen(PORT, (err) => {
  if (err) console.log('## ERROR:', err);
});
