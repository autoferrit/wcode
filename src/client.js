import * as sapper from '@sapper/app'; // eslint-disable-line import/no-unresolved
import 'highlight.js/styles/github.css';

sapper.start({
  target: document.querySelector('#app'),
});
