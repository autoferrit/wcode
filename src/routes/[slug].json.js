import { getArticleContent } from '../app/articles';

export function get(req, res, _next) {
  const { slug } = req.params;

  // get the markdown text
  const { data, html } = getArticleContent(slug);

  if (html) {
    res.writeHead(200, {
      'Content-Type': 'application/json',
    });

    res.end(JSON.stringify({ html, ...data }));
  } else {
    res.writeHead(404, {
      'Content-Type': 'application/json',
    });

    res.end(
      JSON.stringify({
        message: 'Not found',
      })
    );
  }
}
