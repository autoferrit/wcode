import { getArticleData } from '../../app/articles';

export function get(req, res, _next) {
  res.writeHead(200, {
    'Content-Type': 'application/json',
  });
  const posts = getArticleData();
  res.end(JSON.stringify(posts));
}
