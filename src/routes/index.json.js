// import fs from 'fs';
// import grayMatter from 'gray-matter';
// import path from 'path';

// const getAllPosts = () =>
//   fs.readdirSync('content/articles').map((fileName) => {
//     const post = fs.readFileSync(path.resolve('content/articles', fileName), 'utf-8');
//     return grayMatter(post).data;
//   });

// export function get(req, res, _next) {
//   res.writeHead(200, {
//     'Content-Type': 'application/json',
//   });
//   const posts = getAllPosts();
//   res.end(JSON.stringify(posts));
// }

import { getArticleData } from '../app/articles';

export function get(req, res, _next) {
  res.writeHead(200, {
    'Content-Type': 'application/json',
  });
  const posts = getArticleData();
  res.end(JSON.stringify(posts));
}
