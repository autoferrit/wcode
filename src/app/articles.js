import fs from 'fs';
import path from 'path';
import { getFileContent } from './utils/files';
import { parseMDFile, getMDContent } from './utils/markdown';

export function getArticleData() {
  return fs.readdirSync('content/articles').map((fileName) => {
    const mdfile = getFileContent(path.resolve('content/articles', fileName));
    const gray = parseMDFile(mdfile);
    gray.data.excerpt = gray.excerpt;
    return gray.data;
  });
}

export function getArticleContent(slug) {
  const mdfile = fs.readFileSync(
    path.resolve('content/articles', `${slug}.md`),
    'utf-8'
  );
  return getMDContent(mdfile);
}

// export async function getArticles(self) {
//   return self.fetch("articles.json")
//     .then((r) => r.json())
//     .then((articles) => {
//       return { articles }
//     })
// }
