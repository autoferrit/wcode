import fs from 'fs';

export function getFileContent(filepath) {
  return fs.readFileSync(filepath, 'utf-8');
}
