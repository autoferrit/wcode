import grayMatter from 'gray-matter';
import hljs from 'highlight.js';
import marked from 'marked';

const seperator = '<!-- excerpt -->';

function highlightContent() {
  return (source, lang) => {
    if (lang.length === 0) {
      // eslint-disable-next-line no-param-reassign
      lang = 'bash';
    }
    const { value: highlighted } = hljs.highlight(lang, source);
    return `<pre class='language-${lang}'><code>${highlighted}</code></pre>`;
  };
}

export function markdownExcerpt(file, _options) {
  const [newExcerpt, newContent] = file.content.split(seperator);
  // eslint-disable-next-line no-param-reassign
  file.excerpt = newExcerpt;
  // eslint-disable-next-line no-param-reassign
  file.content = newContent;
}

export function parseMDFile(mdfile) {
  return grayMatter(mdfile, {
    excerpt: markdownExcerpt,
    excerpt_separator: seperator,
  });
}

export function getMDContent(mdfile) {
  // function that expose helpful callbacks
  // to manipulate the data before convert it into html
  const renderer = new marked.Renderer();

  // use hljs to highlight our blocks codes
  renderer.code = highlightContent();

  // parse the md to get front matter
  // and the content without escaping characters
  const { data, content, excerpt } = parseMDFile(mdfile);
  data.excerpt = excerpt;

  const html = marked(content, { renderer });

  return { data, html };
}
