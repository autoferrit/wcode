import pkg from '../../../package.json';

export const { name } = pkg;

export const metadata = {
  author: pkg.author,
  url: pkg.url,
  description: pkg.description,
  title: 'Svelte is Awesome',
  image: 'https://svelte.dev/images/twitter-card.png',
  imageAlt: 'Svelte svelte.dev',
};
