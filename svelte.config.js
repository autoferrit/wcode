const sveltePreprocess = require('svelte-preprocess');
const autoprefixer = require('autoprefixer');
const { dev } = require('sapper/dist/rollup');

module.exports = {
  preprocess: sveltePreprocess({
    postcss: true,
    sourceMap: dev,
    // style: async ({ content, attributes }) => {
    //   if (attributes.type !== 'text/postcss') return;
    //   return new Promise((resolve, reject) => {
    //     resolve({ code: '', map: '' });
    //   });
    // },
  }),
};
