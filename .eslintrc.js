module.exports = {
  ignorePatterns: ['__sapper__/'],
  env: {
    browser: true,
    es2020: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'airbnb-base',
    'prettier',
    'plugin:cypress/recommended',
  ],
  overrides: [
    {
      files: ['*.svelte'],
      plugins: ['svelte3'],
      processor: 'svelte3/svelte3',
      rules: {
        'import/first': 'off',
        'import/no-duplicates': 'off',
        'import/no-mutable-exports': 'off',
        'import/no-mutable-unresolved': 'off',
        'no-multiple-empty-lines': 'off',
        'no-undef': 'off',
        'no-unused-vars': 'off',
      },
    },
  ],
  parserOptions: {
    ecmaVersion: 11,
    sourceType: 'module',
  },
  plugins: ['svelte3'],
  rules: {
    'class-methods-use-this': 'off',
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        ts: 'never',
      },
    ],
    'import/no-extraneous-dependencies': 'off',
    'import/prefer-default-export': 'off',
    indent: ['error', 2],
    'max-len': ['error', { code: 88 }],
    'no-console': 'off',
    'no-tabs': [
      'error',
      {
        allowIndentationTabs: false,
      },
    ],
    'no-unused-vars': [
      'error',
      {
        argsIgnorePattern: '^_',
        varsIgnorePattern: '^_',
      },
    ],
    quotes: ['error', 'single'],
  },
};
