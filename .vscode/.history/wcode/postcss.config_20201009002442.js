const postcssImport = require('postcss-import');
const tailwindcss = require('tailwindcss');
const postcssUrl = require('postcss-url');
const prefixer = require('autoprefixer');
const cssnano = require('cssnano');
// const postcssNested = require('postcss-nested');
const postcssSimpleVars = require('postcss-simple-vars');
const postcss = require('postcss');
const postcssPresetEnv = require('postcss-preset-env');

const purgecss = require('@fullhuman/postcss-purgecss')({
  content: ['./src/**/*.svelte', './src/**/*.html'],
  defaultExtractor: (content) =>
    [...content.matchAll(/(?:class:)*([\w\d-/:%.]+)/gm)].map(
      ([_match, group, ..._rest]) => group
    ),
});

const cssFunctions = require('./src/assets/functions.js');
const cssMixins = require('./src/assets/mixins.js');

const mode = process.env.NODE_ENV;
const dev = mode === 'development';

postcss([
  postcssPresetEnv({
    stage: 3,
    importFrom: [
      './src/assets/global.pcss',
      './src/assets/functions.js',
      './src/assets/mixins.js',
    ],
    exportTo: './static/css/main.css',
    features: {
      'nesting-rules': true,
    },
    insertBefore: {
      'all-property': postcssSimpleVars,
    },
    autoprefixer: { grid: true },
  }),
]).process(YOUR_CSS /*, processOptions */);

module.exports = (context) => {
  const { options } = context;

  const plugins = [
    postcssImport({
      path: [options.paths.components, options.paths.styles, 'node_modules'],
    }),
    postcssUrl(),
    tailwindcss('./tailwind.config.js'),
    cssFunctions({ cssFunctions }),
    cssMixins({ cssMixins }),
    prefixer(),
    !dev && purgecss,
    !dev && cssnano({ preset: 'default' }),
  ];

  return {
    plugins,
  };
};
