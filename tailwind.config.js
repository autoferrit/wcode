module.exports = {
  purge: false, // Purging is taken care of in postcss.config.js

  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
  //   experimental: "all";

  future: {
    purgeLayersByDefault: true,
    removeDeprecatedGapUtilities: true,
  },
};
